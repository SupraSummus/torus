MIMUW, JPP 2016, zadanie 4 (smalltalk)

Spacer po torusie
=================

Wprowadzenie
------------
Wszystkie liczby w treści tego zadania są całkowite. Resztą z dzielenia liczby a przez dodatnią, czyli większą od zera, liczbę b nazywamy wartość mod(a, b), spełniającą warunki 0 ≤ mod(a, b) < b oraz a = q b + mod(a, b) dla pewnej liczby q.

Dyskretny torus jest wielowymiarowym uogólnieniem dwukierunkowej listy cyklicznej.

Dla niepustego ciągu dodatnich liczb s ≡ (n1, … nk), torus o kształcie s jest wymiaru k. Elementami tego torusa są punkty, jednoznacznie identyfikowane przez współrzędne (i1, … ik) takie, że 0 ≤ ij < nj dla każdego indeksu 1 ≤ j ≤ k.

Torus nie ma punktów brzegowych. Każdy punkt, po każdej współrzędnej, ma określonego poprzednika i następnika. W torusie o kształcie (n1, … nk) następnikiem punktu o współrzędnych (i1, … ik) po j-tej współrzędnej jest punkt o współrzędnych (i1, … ij - 1, mod(ij + 1, nj), ij + 1, … ik) a poprzednikiem punkt o współrzędnych (i1, … ij - 1, mod(ij - 1, nj), ij + 1, … ik).

Kierunkiem w torusie wymiaru k nazywamy liczbę o wartości bezwzględnej nie przekraczającej k. Kolejnym punktem w kierunku i po punkcie p jest następnik p po i-tej współrzędnej, jeśli i jest dodatnie, poprzednik p po abs(i)-tej współrzędnej, jeśli i jest ujemne oraz punkt p, jeśli i jest równe 0.

Spacer po torusie to, być może nieskończona, kolekcja jego punktów z określoną kolejnością. Kolejne punkty spaceru nie muszą sąsiadować ze sobą w torusie. Spacer może być pusty.

W torusie, traktowanym jak struktura danych, punkty przechowują wartości.

Za reprezentację torusa może służyć dowolny jego punkt.

Polecenie
---------
Zdefiniuj klasę Torus, spełniającą poniższe warunki:

 * Klasa Torus reaguje na komunikat #shape:, z argumentem s będącym niepustą uporządkowaną kolekcją dodatnich liczb, tworząc nowy torus kształtu s i dając jeden z jego punktów.
 * Punkt torusa, w odpowiedzi na komunikat #value, daje przechowywaną przez siebie wartość. Początkową wartością przechowywaną przez punkt jest nil.
 * Komunikat #value:, wysłany do punktu torusa, powoduje zapisanie w nim, jako wartości, obiektu będącego argumentem komunikatu.
 * Metoda #printOn: punktu torusa wypisuje na strumień, który dostaje jako argument, wartość przechowywaną w tym punkcie.
 * Punkt p torusa, w odpowiedzi na komunikat #+ z argumentem j, daje punkt po p w kierunku j.
 * Na komunikat #- z argumentem j punkt torusa reaguje tak, jak na komunikat #+ z argumentem j negated.
 * Wysłanie komunikatu #@ do punktu p torusa wymiaru k, z argumentem będącym uporządkowaną kolekcją liczb v o długości k, daje punkt odległy od p o wektor v. W torusie o kształcie (n1, … nk), dla punktu o współrzędnych (i1, … ik) i wektora (d1, … dk) jest to punkt o współrzędnych (mod(i1 + d1, n1), … mod(ik + dk, nk)).
 * Odpowiedzią na komunikat #| wysłany do punktu p torusa, z argumentem będącym kierunkiem j, jest spacer po wszystkich punktach tego torusa, które mają współrzędne o indeksie innym niż abs(j) takie, jak punkt p. Punkty w tym spacerze nie powtarzają się. Pierwszym punktem spaceru jest p a każdy następny jest kolejnym punktem, po swoim poprzedniku, w kierunku j. Zwracamy uwagę, że dla j = 0, spacer p | j jest jednoelementowy.
 * Dla nieujemnej liczby i oraz kierunku j, do punktu p torusa można wysłać komunikat #% z argumentem i -> j, będącym obiektem standardowej, Smalltalkowej klasy Association. Jako wartość wyrażenia p % (i -> j) dostajemy i-elementowy spacer po torusie zaczynający się od punktu p, w którym każdy następny punkt jest kolejnym, po swoim poprzedniku, w kierunku j. Uwaga - w tak utworzonym spacerze punkty mogą się powtarzać.
 * Metoda #do: spaceru s odwiedza punkty torusa w kolejności określonej przez s. Odwiedzając punkt p, wykonuje ona blok, który jest argumentem #do:, przekazując mu jako argument cały punkt p, a nie przechowywaną przez niego wartość.
 * Spacer s, w odpowiedzi na komunikat #, (przecinek) z argumentem t będącym spacerem, daje sklejenie (konkatenację) spacerów s i t. Punkty spaceru s poprzedzają w niej punkty spaceru t a kolejność punktów w ramach obu sklejanych spacerów jest zachowana. Jeśli spacer s jest nieskończony, odwiedzając punkty w kolejności wyznaczonej przez spacer s , t, do punktów spaceru t nigdy nie dojdziemy.
 * Spacer s rozumie komunikat #| z argumentem x. W odpowiedzi daje sklejenie, z zachowaniem kolejności, wszystkich spacerów będących wynikiem wysłania komunikatu #| z argumentem x do punktów spaceru s.
 * Analogicznie, spacer s w odpowiedzi na komunikat #% z argumentem x daje sklejenie, z zachowaniem kolejności, wszystkich spacerów będących wynikiem wysłania komunikatu #% z argumentem x do punktów spaceru s.
 * Punkt p torusa, w odpowiedzi na komunikat #& z jednoargumentowym blokiem b, daje spacer s. Pierwszym punktem spaceru s jest p. Podczas odwiedzania punktów spaceru s metodą #do:, po odwiedzeniu p obliczana jest wartość w równa b value: p. Jeśli w to nil, spacer się kończy. W przeciwnym przypadku w jest spacerem, którego punkty mają być odwiedzone w następnej kolejności, po punkcie p. Zwracamy uwagę, że metoda ta umożliwia zbudowanie nieskończonego spaceru.
 * Spacer rozumie wszystkie komunikaty odpowiadające metodom klasy Collection. Jego klasowe metody #new i #new: oraz obiektowe metody #add: i #remove:ifAbsent: są zablokowane za pomocą #shouldNotImplement a obiektowa metoda #species daje jako wynik klasę OrderedCollection.

[...]

Wskazówki
---------
Nie trzeba sprawdzać poprawności argumentów komunikatu.

W komunikatach o błędzie, tekstowa reprezentacja obiektu jest budowana za pomocą #printOn:. Metoda #printOn: w klasie Collection korzysta z #do:. Ewentualny błąd w metodzie #do: może więc spowodować kaskadę kolejnych błędów. By tego uniknąć, warto na czas pracy nad programem tymczasowo przedefiniować metodę #printOn: spaceru tak, by nie korzystała z #do:.

Uwagi
-----
Rozwiązanie powinno mieć formę pakietu (pliku .pac) implementacji Dolphin Smalltalk 7. Oprócz definicji klasy Torus, w pakiecie należy umieścić wszystkie potrzebne niestandardowe klasy i metody.

