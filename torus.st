Object subclass: #Torus.

Torus class extend [
	shape: dim [
		^ self new
			dim: dim;
			atPlain: 0.
	]
].

Torus instanceVariableNames: 'dim points'.

Torus extend [
	dim: dimv [ | size |
		dim := dimv.
		size := dim inject: 1 into: [:product :number |
			product * number
		].
		points := (0 to: (size - 1)) collect: [:i |
			TorusPoint new
				torus: self;
				pos: (self pos: i);
				yourself.
		].
	]
	dim [^ dim.]

	atPlain: n [
		^ points at: (n + 1).
	]

	at: v [ | plain |
		plain := 0.
		((dim size) to: 1 by: -1) do: [:i |
			plain := plain * (dim at: i) + ((v at: i) \\ (dim at: i)).
		].
		^ self atPlain: plain.
	]

	pos: plain [ | v plain_ |
		v := Array new: (dim size).
		plain_ := plain.
		(1 to: (dim size)) do: [:i |
			v at: i put: (plain_ \\ (dim at: i)).
			plain_ := plain_ // (dim at: i).
		].
		^ v.
	]
].

Object subclass: #TorusPoint.
TorusPoint instanceVariableNames: 'torus pos val'.

TorusPoint extend [
	torus: v [torus := v.]
	pos: i [pos := i.]

	value: v [val := v.]
	value [^ val.]

	printOn: s [^ val printOn: s.]

	+ j [ | c |
		j = 0 ifTrue: [^ self.].
		c := Array new: (pos size) withAll: 0.
		c at: (j abs) put: (j sign).
		^ self @ c.
	]

	- j [^ self + (j negated).]

	@ v [ |vv|
		vv := Array new: (pos size).
		1 to: (pos size) do: [:i |
			vv at: i put: ((pos at: i) + (v at: i)).
		].
		^ torus at: vv.
	]

	| j [ | b |
		b := [:p | | next |
			next := p + j.
			next = self
				ifTrue: [nil]
				ifFalse: [next & b].
		].
		^ self & b.
	]

	% a [ | j b |
		j := a value.
		b := [:i | [:p |
			i > 1
				ifTrue: [(p + j) & (b value: (i - 1)).]
				ifFalse: [nil.].
		].].
		^ self & (b value: (a key)).
	]

	& b [
		^ SpacerProsty nowy
			point: self;
			nextBlock: b;
			yourself.
	]
].

Collection subclass: #Spacer.

Spacer class extend [
	new [self shouldNotImplement]
	new: v [self shouldNotImplement]
	nowy [^super new]
].

Spacer extend [

	species [^ OrderedCollection.]
	add: v [self shouldNotImplement]
	remove: v ifAbsent: b [self shouldNotImplement]


	first: anInteger [ | answer i |
		answer := OrderedCollection new.
		anInteger > 0 ifFalse: [^answer].
		i := anInteger.
		self do:
			[:each |
			answer add: each.
			i := i - 1.
			i = 0 ifTrue: [^answer]].
		^answer
	]

	, s [
		^ SpacerZlozony nowy
			spacery: {self. s};
			map: [:v | v.];
			yourself.
	]

	| arg [
		^ SpacerZlozony nowy
			spacery: self;
			map: [:p | p | arg];
			yourself.
	]

	% arg [
		^ SpacerZlozony nowy
			spacery: self;
			map: [:p | p % arg];
			yourself.
	]
].


Spacer subclass: #SpacerProsty.
SpacerProsty instanceVariableNames: 'point nextBlock'.

SpacerProsty extend [
	point: p [point := p.]
	nextBlock: p [nextBlock := p.]

	do: block [ | next |
		block value: point.
		next := nextBlock value: point.
		next = nil ifFalse: [next do: block.].
	]
].

Spacer subclass: #SpacerZlozony.
SpacerZlozony instanceVariableNames: 'spacery map'.

SpacerZlozony extend [
	spacery: s [spacery := s.]
	map: m [map := m.]

	do: block [
		spacery do: [:v | (map value: v) do: block].
	]
].
