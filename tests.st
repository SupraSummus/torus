i := Torus shape: #(1 10 3 4 4).

i value: 43.
((i + 0) value = 43) printNl.
((i @ #(0 0 0 0 0)) value = 43) printNl.

(i + 3 + 4 - 3) value: 4.
((i + 4) value = 4) printNl.
((i @ #(0 0 0 1 0)) value = 4) printNl.
((i @ #(0 0 0 5 0)) value = 4) printNl.
((i @ #(0 0 0 -3 0)) value = 4) printNl.
((i - 4 - 4 - 4) value = 4) printNl.
((i + 4 + 4 + 4 + 4 + 4) value = 4) printNl.

t := Torus shape: #(21).
i := 1.
t % (10 -> 1) do: [:p | p value: i. i := i + 1].
i := -1.
(t - 1) % (10 -> -1) do: [:p | p value: i. i := i - 1].
((t | 1 collect: [:p | p value]) asArray = #(1 2 3 4 5 6 7 8 9 10 nil -10 -9 -8 -7 -6 -5 -4 -3 -2 -1)) printNl.

t := Torus shape: #(3 5).
i := 1.
t | 1 | 2 do: [:p | p value: i. i := i + 1].
((t | 2 | 1 collect: [:p | p value]) asArray = #(1 6 11 2 7 12 3 8 13 4 9 14 5 10 15)) printNl.

t := Torus shape: #(7 5).
t | 1 | 2 do: [:p | p value: 0].
i := 1.
t @ #(4 -3) % (4 -> 1) % (2 -> -2) do: [:p | p value: i. i := i + 1].
w := t | 1 collect: [:p | String withAll: (p | 2 collect: [:q | Character digitValue: q value])].
(w asArray = #('08700' '00000' '00000' '00000' '02100' '04300' '06500')) printNl.

t := Torus shape: #(3 2 5).
w := (OrderedCollection new)
	add: t | 1;
	add: t | 2;
	add: t | 3;
	add: t | 1 | 2;
	add: t | 1 | 3;
	add: t | 2 | 3;
	add: t | 1 | 2 | 3;
	yourself.
((w collect: [:c | c size]) asArray = #(3 2 5 6 15 10 30)) printNl.

t := Torus shape: #(5 3).
1 to: 5 do: 
	[:k |
	t value: k.
	t + 2 value: k * 10.
	t + 2 + 2 value: k * 100.
	t := t + 1].
((((t + 1) % (2 -> 1) , (t - 2 | 0)) % (4 -> -2) collect: [:p | p value]) asArray = #(2 200 20 2 3 300 30 3 100 10 1 100)) printNl.

t := Torus shape: #(5).
1 to: 5 do: [:k | t value: k. t := t + 1].
b := nil.
b := [:p | p - 1 - 1 & b].
((((b value: t + 1 + 1 + 1) % (2 -> 1) first: 13) collect: [:p | p value]) asArray = #(2 3 5 1 3 4 1 2 4 5 2 3 5)) printNl.

t := Torus shape: #(5).
1 to: 5 do: [:k | t value: k. t := t + 1].
b := nil.
b := [:p | p value = 2 ifFalse: [p + 1 + 1 & b]].
(((b value: t - 1 - 1) collect: [:p | p value]) asArray = #(1 3 5 2)) printNl.

(((((Torus shape: #(1000)) | 1 | 1 | 1 | 1 | 1) anyOne) value: 11; value) = 11) printNl.
